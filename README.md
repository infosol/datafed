# DataFed [ Dataset Federator Tool ]

### Docker Command Line

```
   docker run -dit --name dataset-federator -p 8080:80 ecoinfo/datafed
```

#### Dataset Federator Link :

```
   http://coby.infosol.inrae.fr/
```
